Raven UWB Build 
==============
Initial recipe for building Intelocks EasyRide solution on Pixel 6 PRO 



Resources:
=========

Google Factory images:
https://developers.google.com/android/images

Google Vendor Binaries:
https://developers.google.com/android/drivers#raven

Building Kernel Guidelines:
https://source.android.com/setup/build/building-kernels

Building AOSP Guidelines:
https://source.android.com/setup/build/downloading

Pixel development Guidelines:
https://source.android.com/devices/automotive/start/pixelxl

Releases and codenames:
https://source.android.com/setup/start/build-numbers#source-code-tags-and-builds

Soong Build System:
https://source.android.com/setup/build


UWB:
=====
https://android-review.googlesource.com/q/uwb

https://android.googlesource.com/kernel/google-modules/uwb/


Others Google tools
=======
CI:
https://ci.android.com/builds/branches/aosp-master-with-phones/grid?

FLash:
https://secure.flash.android.com

Code search (Open Grok like):
https://cs.android.com/
